package data_structures;

public class Binary_Manipulation {
    public static void main(String[] args) {
        byte b = (byte)127;
        System.out.println(b); // should be -128 because overflow

        int i = 1023;
        System.out.println(Integer.toBinaryString(i));
        int j = i << 31;
        System.out.println(j);
        System.out.println(Integer.toBinaryString(j));

    }
}
