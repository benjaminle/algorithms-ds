package data_structures;
import java.util.*;

class Node {
    public Node left_child;
    public Node right_child;
    public Integer value;
    public Integer height;


    public Node(Integer value) {
        left_child = null;
        right_child = null;
        this.height = 0;
        this.value = value;
    }

    public Node (Node left, Node right, Integer value) {
        left_child = left;
        right_child = right;
        this.value = value;
        this.height = Math.max(left.height, right.height) +1;
    }

    public Node getLeft_child() {
        return left_child;
    }

    public Node getRight_child() {
        return right_child;
    }

    public void setLeft_child(Node node) {
        left_child = node;
    }

    public void setRight_child(Node node) {
        right_child = node;
    }
}

class BinarySearchTree {
    public Node rootNode;

    public BinarySearchTree(Node rootNode) {
        this.rootNode = rootNode;
    }

    public Node insertNode(Node node, Integer v) {
        if (node == null ){
            return new Node(v);
        } else {
            if (v < node.value) {
                node.left_child = insertNode(node.left_child, v);
            } else {
                node.right_child = insertNode(node.right_child, v);
            }
            return node;
        }
    }

    public void inorderTraversal(Node node) {
        // also prints ordered list
        if (node != null) {
            inorderTraversal(node.left_child);
            System.out.println(node.value);
            inorderTraversal(node.right_child);
        }
    }

    public void preorderTraversal (Node node) {
        if (node != null ) {
            System.out.println(node.value);
            preorderTraversal(node.left_child);
            preorderTraversal(node.right_child);
        }
    }

    public void postorderTraversal (Node node) {
        // same as DFS
        if (node != null) {
            postorderTraversal(node.left_child);
            postorderTraversal(node.right_child);
            System.out.println(node.value);
        }
    }

    public int height(Node n) {
        if (n == null) {
            return -1;
        } else {
            return Math.max(height(n.left_child), height(n.right_child)) + 1;
        }
    }
    public void specificHeightTraversal (Node n, int level) {
        if (n == null) {
            return;
        }
        if (level == 0) {
            System.out.println(n.value);
        } else {
            specificHeightTraversal(n.left_child, level -1);
            specificHeightTraversal(n.right_child, level -1);
        }
    }
    public void levelorderTraversal () {
        // same as BFS
        for (int level = 0 ; level < height(rootNode) + 1; level++) {
            specificHeightTraversal(rootNode, level);
        }
    }
}

class AVLTree {
    public Node rootNode;

    public Node rotate_left (Node node) {
        if (node.right_child != null ) {
            Node r = node.right_child;
            node.right_child = r.left_child;
            node.height = getNodeHeight(node);
            r.left_child = node;
            r.height = getNodeHeight(r);

            return r;
        } else {
            return node;
        }
    }
    public Node rotate_right (Node node) {
        if (node.left_child != null) {
            Node l = node.left_child;
            node.left_child = l.right_child;
            node.height = getNodeHeight(node);
            l.right_child = node;
            l.height = getNodeHeight(l);
            return l;
        } else {
            return node;
        }
    }

    public Node insert_node (Node node, Integer v) {
        if (node == null) {
            return new Node (v);
        } else {
            if (v < node.value) {
                node.left_child = insert_node(node.left_child, v);
            } else {
                node.right_child = insert_node(node.right_child, v);
            }

            Integer height_diff = heightDiff(node);
            if (height_diff < -1) {
                if (heightDiff(node.right_child) > 0) {
                    node.right_child = rotate_right(node.right_child);
                    node = rotate_left(node);
                } else {
                    node = rotate_left(node);
                }
            } else if (height_diff > 1) {
                if (heightDiff(node.left_child) > 0) {
                    node = rotate_right(node);
                } else {
                    node.left_child = rotate_left(node.left_child);
                    node = rotate_right(node);
                }
            }
            node.height = getNodeHeight(node);
            return node;
        }
    }

    private int getNodeHeight (Node n) {
        return Math.max(n.left_child.height, n.right_child.height) + 1;
    }

    private int heightDiff(Node n) {
        return n.left_child.height - n.right_child.height;
    }
}

class RedBlackTree {
    TreeMap<Integer, Integer> redBlackTree;

}
class Trees{
    public static void main(String[] args) {
        TreeMap<Integer, String> redBlackTree = new TreeMap<>(); // redBlackTree implementation in Java
        redBlackTree.put(10,"ten");
        redBlackTree.put(1, "one");
        redBlackTree.put(3, "three");
        // indeed the keys in Keyset are sorted
        System.out.println(redBlackTree.keySet());

        // Java doesn't have a standard AVL tree implementation
        System.out.println(redBlackTree.ceilingKey(9)); // get the smallest key greater than or equal to parameter
        System.out.println(redBlackTree.floorKey(2)); // get the greatest key less than or equal to parameter


    }
}

