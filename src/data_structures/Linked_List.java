package data_structures;

import java.util.LinkedList;
import java.util.Collections;

public class Linked_List {
    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.add(1);
        linkedList.add(2);

        linkedList.addFirst(0);
        linkedList.addLast(3);
        linkedList.removeFirst();
        linkedList.removeLast();
        linkedList.set(1, 3);
        Integer first = linkedList.getFirst(); // gets a copy of the first value
        Integer last = linkedList.getLast();

        Collections.sort(linkedList);
    }
}
