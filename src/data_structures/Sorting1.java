package data_structures;

import java.util.ArrayList;
import java.util.Arrays;

public class Sorting1 {
    // Quicksort
    public static void quicksort(int[] arr, int l, int r) {
        if (l < r) {
            int partition = partition(arr, l, r);
            quicksort(arr, l, partition - 1);
            quicksort(arr, partition + 1, r);
        }
    }
    public static int partition (int[] arr, int l, int r) { // including r
        int pivot = arr[r];
        int i = l, j = l;
        while (j < r) {
            if (arr[j] < pivot) {
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
            }
            j++;
        }
        int tmp = arr[i];
        arr[i] = pivot;
        arr[r] = tmp;

        return i;
    }

    // Counting sort
    public static void countingSort(int[] arr, int range) {
        int[] freq = new int[range + 1];
        for (int i=  0 ; i < arr.length; i++) {
            freq[arr[i]] += 1;
        }

        int curr = 0;
        for (int i = 0; i < freq.length ; i++) {
            for (int j = 0 ; j < freq[i] ;j++ ) {
                arr[curr] = i;
                curr += 1;
            }
        }
    }

    // Merge sort
    public static void mergeSort(int[] arr, int l, int r) {
        if (l < r) {
            int k = (l+r) / 2;
            mergeSort(arr, l, k);
            mergeSort(arr, k+1, r);
            merge(arr, l, k, r);
        }
    }
    public static void merge (int[] arr, int l, int k, int r) {
        int[] left = Arrays.copyOfRange(arr, l, k+1);
        int[] right = Arrays.copyOfRange(arr, k+1, r+1);

        int left_index = 0, right_index = 0, o = l;
        while (left_index < left.length && right_index < right.length) {
            if (left[left_index] < right[right_index]) {
                arr[o++] = left[left_index++];
            } else {
                arr[o++] = right[right_index++];
            }
        }
        while (left_index < left.length) {
            arr[o++] = left[left_index++];
        }
        while (right_index < right.length) {
            arr[o++] = right[right_index++];
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[] {5,1,4,3,2,2};
        mergeSort(arr, 0, 5);
        for (int elem: arr) {
            System.out.println(elem);
        }
    }
}
