package data_structures;

import java.util.HashMap;
import java.util.Hashtable;

public class Hashmaps {
    public static void main(String[] args) {
        HashMap<Integer, String> hashmap = new HashMap<>(17, 0.75f);
        hashmap.put(10, "Hey");
        hashmap.put(10, "There");
        hashmap.put(120, "Booyakasha");
        hashmap.get(120);
        hashmap.remove(10);
        hashmap.keySet().size();
        hashmap.containsKey(10);

        for (Integer key : hashmap.keySet()) {
            System.out.println(hashmap.get(key));
        }
    }
}
