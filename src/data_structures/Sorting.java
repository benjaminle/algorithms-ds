package data_structures;


import java.util.Arrays;

public class Sorting {
    // QuickSort
    public static void quicksort(int[] arr, int begin, int end) {
        // O(nlogn)
        if (begin < end) {
            int pivotIndex = partition(arr, begin, end);
            quicksort(arr, begin, pivotIndex-1);
            quicksort(arr, pivotIndex+1, end);
        }
    }
    public static int partition (int[] arr, int begin, int end) {
        int pivot = arr[end];
        int i = begin -1;

        for (int j = begin; j < end; j++) {
            if (arr[j] < pivot) {
                i++;
                // swap the elements
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = arr[i];
            }
        }
        int tmp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = tmp;

        return (i+1);
    }

    // CountingSort
    public static void countingsort (int[] arr, int range) {
        // O(n + range)
        // range is the range of possible values that arr[i] can take: 0 to range-1
        int[] counting_array = new int[range];
        for (int i = 0; i < arr.length; i++) {
            counting_array[arr[i]] += 1;
        }

        int total = 0;
        for (int i = 0 ; i < range; i++) {
            for (int j = 0 ; j < counting_array[i]; j++) {
                arr[total] = i;
                total++;
            }
        }

    }

    // MergeSort
    public static void merge (int[] arr, int l, int m, int r) {
        int[] left = Arrays.copyOfRange(arr, l, m+1);
        int[] right = Arrays.copyOfRange(arr, m+1, r+1);
        int i = 0, j = 0, k = l, swaps = 0;
        while (i < left.length && j < right.length) {
            if (left[i] <= right[j]) {
                arr[k++] = left [i++];
            } else {
                arr[k++] = right [j++];
                // swaps += (m+1) - (l+i);
            }
        }
        while (i < left.length) {
            arr[k++] = left[i++];
        }
        while (j < right.length) {
            arr[k++] = right[j++];
        }

        // return swaps;
    }
    public static void mergeSort(int[] arr, int l, int r) {
        // r = arr.length -1 to sort entire array; i.e. l and r are inclusive
        // can be used to count number of inversions in an array

        // inversions = 0;
        if (l < r) {
            int m = (l+r)/2;
            mergeSort(arr, l, m);
            mergeSort(arr, m+1, r);
            merge(arr, l, m, r);
            // inversions += mergeSort(arr, l, m);
            // inversions += mergeSort(arr, m+1, r);
            // inversions += merge(arr, l, m, r);
        }
        // return inversions
    }

    public static void main(String[] args) {
        int[] arr = new int [] {5,4,3,2,1};
        Sorting.countingsort(arr, 6);
        for (int i: arr) {
            System.out.println(i);
        }

    }

}
