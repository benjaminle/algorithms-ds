package data_structures;

import java.util.*;

class UnweightedGraph {
    public HashMap<Integer, ArrayList<Integer>> adjacencyList = new HashMap<>(20);

    public void addVertex (Integer a) {
        adjacencyList.put(a, new ArrayList<>(10));
    }

    public void addEdge (Integer source, Integer destination, boolean bidirectional) {
        if (!bidirectional) {
            addPairVertexToAdjacencyList(source, destination);
        } else {
            addPairVertexToAdjacencyList(source, destination);
            addPairVertexToAdjacencyList(destination, source);
        }
    }

    public void addPairVertexToAdjacencyList(Integer source, Integer destination) {
        ArrayList<Integer> currentList = adjacencyList.get(source);
        if (!currentList.contains(destination)) {
            currentList.add(destination);
            adjacencyList.put(source, currentList);
        }
    }

    public int getVertexCount() {
        return adjacencyList.keySet().size();
    }

    public boolean hasVertex (Integer v) {
        return adjacencyList.keySet().contains(v);
    }

    public boolean hasEdge (Integer s, Integer d) {
        return adjacencyList.get(s).contains(d);
    }

    public int getEdgesCount () {
        int count = 0;
        for (Integer v : adjacencyList.keySet()) {
            count += adjacencyList.get(v).size();
        }
        // bidirectional edge counts as two
        return count;
    }

    public void BFS(Integer source) {
        ArrayList<Integer> visitedVertices = new ArrayList<>(10);
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.add(source);
        Integer focusVertex;
        while (!queue.isEmpty()) {
            focusVertex = queue.peek();
            for (Integer frontier : adjacencyList.get(focusVertex)) {
                if (!(visitedVertices.contains(frontier) || queue.contains(frontier))) {
                    queue.add(frontier);
                }
            }
            System.out.println(focusVertex);
            queue.remove();
            visitedVertices.add(focusVertex);
        }
    }

    private Map<Integer, Integer> parentMap = new HashMap<>();
    public void resetParentMap() {
        parentMap = new HashMap<>();
    }
    public void DFS_Visit(Integer source) {
        for (Integer v : adjacencyList.get(source)) {
            if (!parentMap.containsKey(v)) {
                parentMap.put(v, source);
                DFS_Visit(v);
            }
        }
    }

    public ArrayList<Integer> topological_order = new ArrayList<>();
    public void resetTopologicalOrder() {
        topological_order = new ArrayList<>();
    }
    public void DFS_Visit_Topological(Integer source) {
        for (Integer v : adjacencyList.get(source)) {
            if (!parentMap.containsKey(v)) {
                parentMap.put(v, source);
                DFS_Visit_Topological(v);
            }
        }
        topological_order.add(0, source);
    }
    public ArrayList<Integer> topological_sort(Integer source) {
        resetParentMap();
        resetTopologicalOrder();
        DFS_Visit_Topological(source);
        return topological_order;
    }
}

class WeightedGraph{
    public HashMap<Integer, HashMap<Integer, Float>> adjacencyListWeighted = new HashMap<>();

    public int edgesCount () {
        int count = 0;
        for (Integer v : adjacencyListWeighted.keySet()) {
            System.out.println("Vertex " + v + " has " + adjacencyListWeighted.get(v).keySet().size());
            count += adjacencyListWeighted.get(v).keySet().size();
        }
        return count;
    }
    public void addVertex(Integer v) {adjacencyListWeighted.put(v, new HashMap<>(10));}
    public void addWeightedEdge(Integer source, Integer destination, Float weight, boolean bidirectional) {
        if (!bidirectional) {
            HashMap<Integer, Float> current_list = adjacencyListWeighted.get(source);
            current_list.put(destination, weight);
            adjacencyListWeighted.put(source, current_list);
        } else {
            HashMap<Integer, Float> current_list = adjacencyListWeighted.get(source);
            current_list.put(destination, weight);
            adjacencyListWeighted.put(source, current_list);

            HashMap<Integer, Float> current_list1 = adjacencyListWeighted.get(destination);
            current_list1.put(source, weight);
            adjacencyListWeighted.put(destination, current_list1);
        }
    }
    public Float dijkstra (Integer source, Integer destination) {
        HashMap<Integer, Float> unknown_vertices = new HashMap<>();
        HashMap<Integer, Float> determined_vertices = new HashMap<>();
        for (Integer i : adjacencyListWeighted.keySet()) {
            if (i == source) {
                unknown_vertices.put(i, 0F);
            } else {
                unknown_vertices.put(i, Float.MAX_VALUE);
            }
        }

        while (!unknown_vertices.isEmpty()) {
            Integer u = Integer.MAX_VALUE;
            Float current_smallest = Float.MAX_VALUE;
            for (Integer vertex : unknown_vertices.keySet()) {
                if (unknown_vertices.get(vertex) < current_smallest) {
                    u = vertex;
                    current_smallest = unknown_vertices.get(vertex);
                }
            }
            if (u == destination) {
                return unknown_vertices.get(u);
            }

            // Relaxing all edges from u
            for (Integer d : adjacencyListWeighted.get(u).keySet()) {
                if (!determined_vertices.keySet().contains(d)) {
                    Float weight_from_u = unknown_vertices.get(u) + adjacencyListWeighted.get(u).get(d);
                    if (weight_from_u < unknown_vertices.get(d)){
                        unknown_vertices.put(d, weight_from_u);
                    }
                }
            }
            unknown_vertices.remove(u);
            determined_vertices.put(u, current_smallest);
        }
        return determined_vertices.get(destination);
    }

    public Float bellman_ford (Integer source, Integer destination) {
        HashMap<Integer, Float> current_best = new HashMap<>();

        for (Integer v : adjacencyListWeighted.keySet()) {
            if (v != source) {
                current_best.put(v, Float.MAX_VALUE);
            } else {
                current_best.put(v, 0F);
            }
        }

        for (int i =0 ; i < adjacencyListWeighted.keySet().size(); i++) {
            for (Integer s : adjacencyListWeighted.keySet()) {
                for (Integer d : adjacencyListWeighted.get(s).keySet()) {
                    if (current_best.get(s) != Float.MAX_VALUE) {
                        Float tmp = current_best.get(s) + adjacencyListWeighted.get(s).get(d);
                        if (tmp < current_best.get(d)) {
                            current_best.put(d, tmp);
                        }
                    }
                }
            }
        }

        for (Integer s: adjacencyListWeighted.keySet()) {
            for (Integer d : adjacencyListWeighted.get(s).keySet()){
                if (current_best.get(s) + adjacencyListWeighted.get(s).get(d) < current_best.get(d)){
                    return Float.MIN_VALUE; // has negative cycle
                }
            }
        }
        return current_best.get(destination);
    }

}

public class    Graphs {
    public static void main(String[] args) {
        // example graph: https://www.geeksforgeeks.org/implementing-generic-graph-in-java/
        UnweightedGraph g = new UnweightedGraph();
        g.addVertex(0);
        g.addVertex(1);
        g.addVertex(2);
        g.addVertex(3);
        g.addVertex(4);
        g.addEdge(0,1, true);
        g.addEdge(0,4, true);
        g.addEdge(1,4, true);
        g.addEdge(1,3, true);
        g.addEdge(4,3, true);
        g.addEdge(2,3, true);
        g.addEdge(1,2, true);

        g.BFS(0);


        // https://www.statisticshowto.com/directed-acyclic-graph/
        UnweightedGraph acyclicGraph = new UnweightedGraph();
        for (int i = 1; i <= 7; i++) {
            acyclicGraph.addVertex(i);
        }
        acyclicGraph.addEdge(1, 2, false);
        acyclicGraph.addEdge(2, 4, false);
        acyclicGraph.addEdge(2, 5, false);
        acyclicGraph.addEdge(4, 7, false);
        acyclicGraph.addEdge(5, 7, false);
        acyclicGraph.addEdge(1, 3, false);
        acyclicGraph.addEdge(3, 6, false);
        acyclicGraph.addEdge(6, 5, false);
        acyclicGraph.addEdge(6, 7, false);

        System.out.println("Topological sort");
        ArrayList<Integer> topological_order = acyclicGraph.topological_sort(1);
        System.out.println(topological_order);

        // https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-using-priority_queue-stl/
        System.out.println("Dijkstra");
        WeightedGraph dijkstra = new WeightedGraph();
        for (int i = 0; i < 9; i++) {
            dijkstra.addVertex(i);
        }
        dijkstra.addWeightedEdge(0, 1, 4F, true);
        dijkstra.addWeightedEdge(0, 7, 8F, true);
        dijkstra.addWeightedEdge(1, 7, 11F, true);
        dijkstra.addWeightedEdge(7, 6, 1F, true);
        dijkstra.addWeightedEdge(7, 8, 7F, true);
        dijkstra.addWeightedEdge(6, 8, 6F, true);
        dijkstra.addWeightedEdge(6, 5, 2F, true);
        dijkstra.addWeightedEdge(1, 2, 8F, true);
        dijkstra.addWeightedEdge(2, 8, 2F, true);
        dijkstra.addWeightedEdge(2, 5, 4F, true);
        dijkstra.addWeightedEdge(2, 3, 7F, true);
        dijkstra.addWeightedEdge(3, 5, 14F, true);
        dijkstra.addWeightedEdge(3, 4, 9F, true);
        dijkstra.addWeightedEdge(4, 5, 10F, true);
        System.out.println(dijkstra.edgesCount());
        System.out.println(dijkstra.dijkstra(0, 3));

        // https://www.geeksforgeeks.org/bellman-ford-algorithm-dp-23/
        System.out.println("Bellman-Ford");
        WeightedGraph bf = new WeightedGraph();
        for (int i = 0 ; i < 5; i++) {
            bf.addVertex(i);
        }
        bf.addWeightedEdge(0, 1, -1F, false);
        bf.addWeightedEdge(0, 2, 4F, false);
        bf.addWeightedEdge(1, 2, 3F, false);
        bf.addWeightedEdge(3, 2, 5F, false);
        bf.addWeightedEdge(1, 3, 2F, false);
        bf.addWeightedEdge(3, 1, 1F, false);
        bf.addWeightedEdge(1, 4, 2F, false);
        bf.addWeightedEdge(4, 3, -2F, false);
        System.out.println(bf.edgesCount());
        System.out.println(bf.bellman_ford(0, 2));
    }
}
