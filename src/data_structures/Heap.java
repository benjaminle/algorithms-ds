package data_structures;

import java.util.ArrayList;

public class Heap {
    public static void main(String[] args) {
        MaxHeap mh = new MaxHeap();
        for (int i = 0 ; i < 6; i++) {
            mh.insert(i);
        }

        mh.max_heap();
        for (int i = 1 ; i < mh.Heap.size(); i++) {
            System.out.println(mh.Heap.get(i));
        }

        for (int i = 0 ; i < 4; i++) {
            System.out.println("pop " + mh.pop());
            for (int j = 1; j < mh.Heap.size(); j++) {
                System.out.println(mh.Heap.get(j));
            }
        }

        System.out.println("MHS");
        MaxHeap mhs = new MaxHeap(true);
        for (int i = 1 ; i < mhs.Heap.size() ; i++) {
            System.out.println(mhs.Heap.get(i));
        }
        mhs.max_heap();
        for (int i = 1 ; i < mhs.Heap.size() ; i++) {
            System.out.println(mhs.Heap.get(i));
        }

    }
}

class MaxHeap {
    public ArrayList<Integer> Heap;

    public MaxHeap() {
        this.Heap = new ArrayList<>();
        Heap.add(Integer.MAX_VALUE);
    }

    public MaxHeap(boolean standard) {
        Heap = new ArrayList<>();
        Heap.add(Integer.MAX_VALUE);
        for (int i =1 ; i < 6; i++) {
            Heap.add(i);
        }
    }

    public void swap (int pos1, int pos2) {
        Integer tmp = Heap.get(pos1);
        Heap.set(pos1, Heap.get(pos2));
        Heap.set(pos2, tmp);
    }

    public Integer pop() {
        Integer return_value = Heap.get(1);
        swap(1, Heap.size() - 1);
        Heap.remove(Heap.size()-1);
        max_heapify(1);
        return return_value;
    }

    public void max_heapify (int pos) {
        /*
        Correct "local error" and move down the tree
        Doesn't necessarily correct the entire tree
         */
        if (pos > 0) {
            String leaf_classification = position_classifier(pos);
            if (leaf_classification == "both leaves exist") {
                if (Heap.get(pos) < Heap.get(2*pos) || Heap.get(pos) < Heap.get(2*pos+1)) {
                    int larger_of_left_or_right = (Heap.get(2 * pos) > Heap.get(2 * pos + 1)) ? 1 : 0;
                    if (larger_of_left_or_right == 1) {
                        // left is larger
                        swap(pos, 2 * pos);
                        max_heapify(2 * pos);
                    } else {
                        swap(pos, 2 * pos + 1);
                        max_heapify(2 * pos + 1);
                    }
                }
            } else if (leaf_classification == "leaf") {
            } else if (leaf_classification == "left exist") {
                if (Heap.get(pos) < Heap.get(2*pos)) {
                    swap(pos, 2 * pos);
                    max_heapify(2 * pos);
                }
            }
        }
    }

    public String position_classifier(int pos) {
        if (pos >= Heap.size()) {
            return "out of index";
        } else {
            if (2 * pos >= Heap.size()) {
                return "leaf";
            } else if (2*pos + 1 >= Heap.size()){
                return "left exist";
            } else {
                return "both leaves exist";
            }
        }
    }
    public void max_heap () {
        /*
        Correct the entire tree from unordered array
         */
        for (int i = Heap.size()/2 + 1 ; i > 0; i--) {
            max_heapify(i);
        }
    }

    public void insert (Integer value) {
        Heap.add(value);
        Integer index = Heap.size() - 1;
        while (Heap.get(index) > Heap.get(index/2)) {
            swap(index, index/2);
            index = index/2;
        }
    }
}
