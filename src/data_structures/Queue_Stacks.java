package data_structures;

import java.util.*;

public class Queue_Stacks {
    public static void main(String[] args) {
        // Queues: FIFO
        Queue<Integer> priorityQueue = new PriorityQueue<>(11, Collections.reverseOrder());
        // priorityQueue implements a heap
        priorityQueue.add(1);
        priorityQueue.add(4);
        priorityQueue.add(3);
        System.out.println(priorityQueue);
        priorityQueue.remove(1); // removes object
        priorityQueue.remove(); // remove first of queue
        System.out.println(priorityQueue);
        System.out.println(priorityQueue.peek()); // gets the "largest element" base on the comparator
        System.out.println(priorityQueue.poll()); // gets the "largest element" base on the comparator and remove it
        // Looping: 2 ways
        Object[] array = priorityQueue.toArray();
        for (Object i : array) {
            System.out.println(i);
        }

        Iterator iterator = priorityQueue.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }

        Queue<Integer> linkedList = new LinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.remove();
        System.out.println(linkedList.peek());
        System.out.println(linkedList);


        // Stack: LIFO
        System.out.println("Stack");
        Stack<Integer> stack = new Stack<Integer>();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.insertElementAt(4, 3);
        System.out.println(stack.search(1)); // top of stack is index 0
        stack.peek();
        stack.elementAt(0);
        stack.pop(); // return and remove the top element of the stack

    }
}


