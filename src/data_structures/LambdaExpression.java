package data_structures;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;

public class LambdaExpression {
    public static void main(String[] args) {
        Consumer<Integer> printLambda = (n) -> {System.out.println(n);};
        
        ArrayList<Integer> ar = new ArrayList();
        ar.add(1);
        ar.add(2);
        ar.add(3);
        ar.add(4);
        ar.forEach(printLambda);

    }
}
