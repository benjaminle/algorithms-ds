package data_structures;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

class Sample {
    public int value = 0;

    Sample(int value) {
        this.value = value;
    }

    public void setValue(int k) {
        value = k;
    }
    public void setValue(Sample s) {
        s.value = 10;
    }

    @Override
    public int hashCode() {
        return value;
    }
}

public class testing {


    public static void main(String[] args) {

        String str = "abc";
        char[] charArray = new char[26]; // this is used to store data
        for (char a: str.toCharArray()) {
            charArray[a -  'a'] += 90;
        }
        String key = String.valueOf(charArray);
        System.out.println(key);
        for (int i = 0 ; i < charArray.length; i++) {
            System.out.println(charArray[i]);
        }
    }
}
