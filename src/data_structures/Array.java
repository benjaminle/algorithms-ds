package data_structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.net.*;

class Array {
    public static void main(String[] args) {
        ArrayList<Integer> arrayOfInts = new ArrayList<>(12);
        arrayOfInts.add(1);
        arrayOfInts.get(0);
        arrayOfInts.set(0, 2);
        arrayOfInts.remove(0); // remove item at index 0
        arrayOfInts.clear(); // remove all items from array
        arrayOfInts.size(); // return an integer size
        for (Integer i : arrayOfInts) { // cannot directly change items in arrayOfInts
            System.out.println(i);
        }
        for (int i = 0 ; i < arrayOfInts.size(); i++) {
            System.out.println(arrayOfInts.get(i));
        }

        arrayOfInts.add(1);
        arrayOfInts.add(2);
        arrayOfInts.add(4);
        arrayOfInts.add(3);
        Collections.sort(arrayOfInts); // sort in ascending order
        Collections.sort(arrayOfInts, Collections.reverseOrder()); // sort in descending order
        Collections.sort(arrayOfInts, new GreaterThanComparator()); // sort in ascending order

        String[] words = {"ba", "a", "abc"};
        Arrays.sort(words, (a,b) -> {
            return a.length() - b.length();
        });
        for (int i = 0 ; i < words.length; i++) {
            System.out.println(words[i]);
        }
    }
}

class GreaterThanComparator implements Comparator {
    public int compare (Object o1, Object o2) {
        Integer i1 = (Integer) o1;
        Integer i2 = (Integer) o2;
        if (i1 == i2) {
            return 0;
        } else if (i1 > i2) {
            return 1;
        } else {
            return -1;
        }
    }
}

class LessThanComparator implements Comparator {
    public int compare (Object o1, Object o2) {
        Integer i1 = (Integer) o1;
        Integer i2 = (Integer) o2;
        if (i1 == i2) {
            return 0;
        } else if (i1 < i2) {
            return 1;
        } else {
            return -1;
        }
    }
}
